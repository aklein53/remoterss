﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RemoteRssService
{
	[ServiceContract]
	public interface IRemoteRSS
	{
		[OperationContract]
		[ServiceKnownType(typeof(RecoImage))]
		[WebInvoke(
			UriTemplate = "reco",
			Method = "POST",
			BodyStyle = WebMessageBodyStyle.Bare,
			RequestFormat = WebMessageFormat.Json,
			ResponseFormat = WebMessageFormat.Json)]
		RecoResult RecoCheck(RecoImage image);
	}

	[DataContract]
	public class RecoImage
	{
		byte[] imageData = new byte[0];

		/// <summary>
		/// A Base-64 encoded string
		/// </summary>
		[DataMember]
		public string ImageDataString
		{
			get { return Convert.ToBase64String(imageData); }
			set { imageData = Convert.FromBase64String(value); }
		}

		public byte[] ImageData
		{
			get { return imageData; }
			set { imageData = value; }
		}
	}

	[DataContract]
	public class RecoResult
	{
		Decimal amount = 0.00M;
		string micr = "";
		byte[] imageData = new byte[0];

		[DataMember]
		public Decimal Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		[DataMember]
		public string MICR
		{
			get { return micr; }
			set { micr = value; }
		}
		
		/// <summary>
		/// A Base-64 encoded string
		/// </summary>
		[DataMember]
		public string ImageDataString
		{
			get { return Convert.ToBase64String(imageData); }
			set { imageData = Convert.FromBase64String(value); }
		}

		public byte[] ImageData
		{
			get { return imageData; }
			set { imageData = value; }
		}
	}
}
