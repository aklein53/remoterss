﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Diebold.Agilis.RSS;
using System.IO;
using System.Drawing;
using System.Security.Cryptography;

namespace RemoteRssService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class RemoteRSS : IRemoteRSS
	{
		public RecoResult RecoCheck(RecoImage image)
		{
			RecoResult result = new RecoResult();

			bool success = true;
			bool amountSuccess = false;
			bool micrSuccess = false;

			MemoryStream ms = new MemoryStream();
			ms.Write(image.ImageData, 0, image.ImageData.Count());

			System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(ms);
			bitmap.SetResolution(200, 200);

			bitmap.Save("testRecoImage.bmp", System.Drawing.Imaging.ImageFormat.Bmp);

			IAgilisRSS recoEngine = new AgilisRSS();

			recoEngine.BothOrientations = true;
			recoEngine.MinimumConfidence = 60;
			recoEngine.TopImageFileName = "testRecoImage.bmp";
			recoEngine.ImageType = RSSImageType.BMP;
			recoEngine.TemplateName = "US_AMOUNT";
			recoEngine.InitialRotation = 0;

			try
			{
				recoEngine.DoRecognizeSync();
				string recoResult = recoEngine.RecognizeResult;

				result.Amount = Decimal.Parse(recoResult) / 100;

				amountSuccess = true;
			}
			catch (Exception ex)
			{
				result.Amount = 0.0m;
				success = false;
			}

			recoEngine.TemplateName = "US_E13B";

			try
			{
				recoEngine.DoRecognizeSync();
				string recoResult = recoEngine.RecognizeResult;
				
				result.MICR = recoResult;

				micrSuccess = true;
			}
			catch (Exception ex)
			{
				result.MICR = "Not found";
				success = false;
			}

			FileStream fs = new FileStream("testRecoImage.bmp", FileMode.Open);
			byte[] imageData = new byte[fs.Length];
			fs.Read(imageData, 0, (int)fs.Length);

			result.ImageData = imageData;

			/*if (!success) // If we couldn't reco everything, we'll flip the image and try again
			{
				success = true;

				bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
				bitmap.Save("testRecoImage.bmp", System.Drawing.Imaging.ImageFormat.Bmp);

				if (!amountSuccess) // If amount was not successfully read
				{
					recoEngine.TemplateName = "US_AMOUNT";

					try
					{
						recoEngine.DoRecognizeSync();
						string recoResult = recoEngine.RecognizeResult;

						result.Amount = Decimal.Parse(recoResult) / 100;
					}
					catch (Exception ex)
					{
						result.Amount = 0.0m;
						success = false;
					}
				}

				if (!micrSuccess)
				{
					recoEngine.TemplateName = "US_E13B";

					try
					{
						recoEngine.DoRecognizeSync();
						string recoResult = recoEngine.RecognizeResult;

						result.MICR = recoResult;

						micrSuccess = true;
					}
					catch (Exception ex)
					{
						result.MICR = "Not found";
						success = false;
					}
				}

				if (success)
				{
					result.IsUpsideDown = true;
				}
			}*/

			return result;
		}
	}
}
