﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace RemoteRSS
{
	class RSSServiceHost
	{
		private RemoteRssService.RemoteRSS serv;
		private WebServiceHost host;

		public void Start()
		{
			this.serv = new RemoteRssService.RemoteRSS();

			this.host = new WebServiceHost(this.serv);
			this.host.Faulted += host_Faulted;
			this.host.UnknownMessageReceived += host_UnknownMessageReceived;
			this.host.Open();

			Console.WriteLine("Service started and listening for connections on:");
			foreach (ServiceEndpoint endpt in this.host.Description.Endpoints)
			{
				Console.WriteLine("{0} {1}", endpt.Address.ToString(), endpt.Binding.Name);
			}
		}

		void host_UnknownMessageReceived(object sender, UnknownMessageReceivedEventArgs e)
		{
			Console.WriteLine(e.Message);
		}

		void host_Faulted(object sender, EventArgs e)
		{
			Console.WriteLine(e.ToString());
		}
	}
}
