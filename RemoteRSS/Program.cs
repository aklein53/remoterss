﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteRSS
{
	class Program
	{
		static void Main(string[] args)
		{
			RSSServiceHost host = new RSSServiceHost();
			host.Start();

			Console.ReadKey();
		}
	}
}
